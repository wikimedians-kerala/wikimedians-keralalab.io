#!/usr/bin/env python
# -*- coding: utf-8 -*- #
from __future__ import unicode_literals
from jinja2 import contextfilter, Template
from markupsafe import Markup

AUTHOR = u'WoKUG'
SITENAME = u'Wikimedians of Kerala'
SITEURL = ''
THEME = 'themes/evie'
PATH = 'content'
OUTPUT_PATH = 'public'
DELETE_OUTPUT_DIRECTORY = True
TIMEZONE = 'Asia/Kolkata'
DEFAULT_LANG = u'en'

#DISPLAY_PAGES_ON_MENU = True
DISPLAY_CATEGORIES_ON_MENU = False

MENUITEMS = (
    ('Home', '/'),
    )

STATIC_PATHS = ['events']
PAGE_URL = '{slug}/index.html'
PAGE_SAVE_AS = '{slug}/index.html'
SLUGIFY_SOURCE = 'basename'

# Feed generation is usually not desired when developing
FEED_ALL_ATOM = None
CATEGORY_FEED_ATOM = None
TRANSLATION_FEED_ATOM = None
AUTHOR_FEED_ATOM = None
AUTHOR_FEED_RSS = None

# Blogroll
#LINKS = (('Pelican', 'http://getpelican.com/'),
#         ('Python.org', 'http://python.org/'),
#         ('Jinja2', 'http://jinja.pocoo.org/'),
#         ('You can modify those links in your config file', '#'),)

# Social widget
#SOCIAL = (('You can add links in your config file', '#'),
#          ('Another social link', '#'),)

DEFAULT_PAGINATION = 5

# Uncomment following line if you want document-relative URLs when developing
#RELATIVE_URLS = True

# PLUGINS
PLUGIN_PATHS = ['plugins']
PLUGINS = ['pelican-page-hierarchy']

@contextfilter
def st_render(context, value):
    return Markup(Template(value).render(context)).render()

JINJA_FILTERS = {'strender':st_render}
