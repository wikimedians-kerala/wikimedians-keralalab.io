Title: About
Container: True

<div class="steps">
Wikimedians of Kerala is a group of Wikimedians which work on various wikimedia projects related to Malayalam and other languages. The group is also working with other organizations to promote Malayalam langauge. Anyone can become a member of this group.
</div>

## More Details

  Item              | Description
  ------------------| -------------
  Location          | Kerala
  Country           | India
  Affiliation       | Wikimedia Foundation
  Founding Date     | August 27, 2018
  Approval Date     | October 17, 2018
  Official Language | Malayalam
