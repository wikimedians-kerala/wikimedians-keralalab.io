Title: Wikipedia Workshop at Sree Sankaracharya University of Sanskrit, Kalady
Status: Hidden
Slug: ssus_kalady
Container: True
Featured_img:https://upload.wikimedia.org/wikipedia/commons/thumb/7/7b/Academic_block_No1_Sree_Sankaracharya_University_of_Sanskrit.jpg/1024px-Academic_block_No1_Sree_Sankaracharya_University_of_Sanskrit.jpg
Gallery: 20190208_ssus_kalady
Gallery_items: 6
Gallery_links: Kannan V M introducing Wikipedia to Malayalam Department Students at Sree Sankaracharya University of Sanskrit Kalady 02.jpg|Malayalam Department Students at Wikimedia Workshop, Sree Sanakaracharya University of Sanskrit, Kalady.jpg|Ranjith Siji introducing Malayalam Wikipedia to Malayalam Department Students at Sree Sankaracharya University of Sanskrit Kalady.jpg|Wikimedia Workshop at Sree Sankaracharya University of Sanskrit 2.jpg|Wikimedia Workshop at Sree Sankaracharya University of Sanskrit44.jpg|Wikimedia Workshop in association with Malayalam Department, Sree Sankaracharya University of Sanskrit, Kalady.jpg

<div class="steps">
A Wikimedia workshop is conducted at Sree Sankaracharya University of Sanskrit, Kalady on 8th February 2019. This workshop is conducted in collaboration with Malayalam Department in the University. 9 edits done on malayalam wikipedia and 5 new wikidata lexemes created. 
</div>

## Highlights

  Item           | Description
  -------------  | -------------
  Event          | Wikipedia Workshop
  Date           | February 8, 2019
  Venue          | Sree Sankaracharya University of Sanskrit (SSUS), Kalady
  Location       | Kalady, Ernakulam District, Kerala
  Coordinators   | Ranjith Siji, Kannan V M
  Participants   | Malayalam Department, SSUS Kalady
  Organized by   | Wikimedians of Kerala User Group
  New Users      | 23
  Total Edits    | 12


## Team

### Coordinators
* Ranjith Siji
* Kannan V M
* Ambady Anand S

### Participants
* Vrindasreejith
* Noufal machakkulam
* LIJIN PUTHENPURAYIL
* മേരി റീമ
* Krsajithaa
* Sureshsaaketham
* യദു കൃഷ്ണൻ കെഎം
* Thalasseryvibe
* Snehabalandevabalan
* Athul Balakrishnan Blathur
* RASHIDHA PALAMMOOTTIL
* Valarmathimaya
* Nowfal blathur
* SHYJU K THENNALA
* Anusree Chandran
* Alphons thopramkudy
* Uwise M S
* Bhagya Thadathil
* BIYASADHAN
* Salim Sangeeth
* Santhosh Indeevaram
* NEETHU UNNI
* VIDYA ARSHO
* അനിത സാബു
