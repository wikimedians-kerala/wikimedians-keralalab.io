Title: Introduction to Wikipedia at  College of Engineering, Karunagappally
Status: Hidden
Slug: coe_karunagappally
Container: True
Featured_img: https://upload.wikimedia.org/wikipedia/commons/thumb/8/88/College_of_engineering_karunagapally_DSC_3480.jpg/1024px-College_of_engineering_karunagapally_DSC_3480.jpg
Gallery: 20190217_coe_karunagappally
Gallery_items: 2
Gallery_links: College of engineering karunagapally DSC 3486.jpg|Introduction to Wikipedia at College of engineering karunagapally DSC 3536.jpg

<div class="steps">
A Wikipedia workshop was conducted at College of Engineering Karunagappally on 17th February 2019. Students from Computer science, Electronics and Communication, Electrical and Electronics are participated in this workshop. Ranjithsiji introduced Wikidata and Mujeeb Rahman introduced Wikipedia, Commons and other Wikimedia projects to the participants at this workshop. 30 Students participated in the event.
</div>

## Highlights

  Item           | Description
  -------------  | -------------
  Event          | Introduction to Wikipedia
  Date           | February 17, 2019
  Venue          | College of Engineering, Karunagappally
  Location       | Karunagappally, Kollam District, Kerala
  Coordinators   | Ranjith Siji, Mujeeb Rahman, Kiran S Kunjumon
  Participants   | Computer Science and Engineering Department, CoE Karunagappally
  Organized by   | Wikimedians of Kerala User Group

## Team

### Coordinators
* Ranjith Siji
* Mujeeb Rahman
* Kiran S Kunjumon
