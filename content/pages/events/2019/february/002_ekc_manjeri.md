Title: Introduction to Wikipedia at Eranad Knowledge City, Manjeri
Status: Hidden
Slug: ekc_manjeri
Container: True
Featured_img: https://upload.wikimedia.org/wikipedia/commons/0/0f/Knowledge_of_city_wiki_introduction_by_mujeeb.jpg
Gallery: 20190204_ekc_manjeri
Gallery_items: 2
Gallery_links: Knowledge of city wiki introduction by mujeeb.jpg|Introduction to Wikipedia by Mujeeb Rahman at Eranad Knowledge City.jpg

<div class="steps">
An Introduction to Wikipedia and Wikimedia projects is done at Eranad Knowledge City, Manjeri on 4th February 2019. 34 Students of Computer Science department participated in the event. Mujeeb Rahman given an introduction about Wikipedia and Wikimedia Commons in this training. A small interactive session about the autheticity of Wikipedia and copyright about the content in Wikipedia and Commons is done.
</div>

## Highlights

  Item           | Description
  -------------  | -------------
  Event          | Introduction to Wikipedia
  Date           | February 4, 2019
  Venue          | Eranad Knowledge City (EKC), Manjeri
  Location       | Manjeri, Malappuram District, Kerala
  Coordinators   | Mujeeb Rahman, Sandini Sukumar
  Participants   | Computer Science and Engineering Department, EKC Manjeri
  Organized by   | Wikimedians of Kerala User Group
  
  
## Team
  
### Coordinators
* Mujeeb Rahman
* Sandini Sukumar
