Title: Introduction to Wikipedia at Ahalia School of Engineering and Technology, Palakkad
Status: Hidden
Slug: aset_palakkad
Container: True
Featured_img: https://upload.wikimedia.org/wikipedia/commons/thumb/9/91/Ahalia_School_of_Engineering_and_Technology_Palakkad_04.jpg/1024px-Ahalia_School_of_Engineering_and_Technology_Palakkad_04.jpg
Gallery: 20190215_aset_palakkad
Gallery_items: 3
Gallery_links: Mujeeb Rahman introducing Wikipedia to students at Ahalia School of Engineering and Technology Palakkad.jpg|Mujeeb Rahman introducing Wikimedia Commons at Ahalia School of Engineering and Technology Palakkad.jpg|Students at Wikipedia Workshop, Ahalia School of Engineering and Technology, Palakkad.jpg

<div class="steps">
A Wikipedia workshop was conducted at Ahalia School of Engineering and Technology (ASET), Palakkad on 15th February 2019. This workshop was conducted in collaboration with the ASET Computer Science and Engineering (CSE) Department. 
</div>

## Highlights

  Item           | Description
  -------------  | -------------
  Event          | Introcution to Wikipedia
  Date           | February 15, 2019
  Venue          | Ahalia School of Engineering and Technology (ASET), Palakkad
  Location       | Kozhippara, Palakkad District, Kerala
  Coordinators   | Mujeeb Rahman, Ambady Anand S
  Participants   | Computer Science and Engineering Department, ASET Palakkad
  Organized by   | Wikimedians of Kerala User Group

## Team

### Coordinators
* Mujeeb Rahman
* Ambady Anand S
