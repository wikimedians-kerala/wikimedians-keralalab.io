Title: Wikimedia Exhibition and Stall at Dhishna Tech Fest, Cochin University of Science and Technology, Kochi
Status: Hidden
Slug: cusat_kochi
Container: True
Featured_img: https://upload.wikimedia.org/wikipedia/commons/thumb/5/5c/Administrative_office%2C_CUSAT.jpg/1024px-Administrative_office%2C_CUSAT.jpg
Gallery: 20190202_cusat_kochi
Gallery_items: 6
Gallery_links: Wikimedia Exhibition at Cochin University of Science and Technology DSC 2066.jpg|Wikimedia Exhibition at Cochin University of Science and Technology DSC 2045.jpg|Wikimedia Exhibition at Cochin University of Science and Technology DSC 2030.jpg|Wikimedia Exhibition at Cochin University of Science and Technology DSC 2077.jpg|Wikimedia Exhibition at Cochin University of Science and Technology DSC 2087.jpg|Wikimedia Exhibition at Cochin University of Science and Technology DSC 2068.jpg

<div class="steps">
A one day exhibition about Wikimedia projects and a stall was organized at Dhishna, the Tech fest of Cochin University of Science and Technology on 2nd February 2019. Ranjithsiji, Ambadyanands, Kannan VM, Mujeeb Rahman are participated in the stall to explain about Wikipedia, Commons and other wikimedia projects to the stdents and other people who visit the stall. The event started on 10AM and the stall closed on 4:30PM. Stickers and other swags are distributed at the stall.
</div>

  Item           | Description
  -------------  | -------------
  Event          | Exhibition and Stall
  Date           | February 2, 2019
  Venue          | Cochin University of Science and Technology (CUSAT), Kochi
  Location       | Kochi, Ernakulam District, Kerala
  Coordinators   | Ranjith Siji, Mujeeb Rahman, Kannan V M, Ambady Anand S, Kiran S Kunjumon
  Participants   | Computer Science and Engineering Department, CUSAT Kochi
  Organized by   | Wikimedians of Kerala User Group
  
## Team

### Coordinators
* Ranjith Siji
* Mujeeb Rahman
* Kannan V M
* Ambady Anand S
* Kiran S Kunjumon
