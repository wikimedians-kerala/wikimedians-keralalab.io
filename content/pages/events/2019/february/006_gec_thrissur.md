Title: Wikidata Workshop at Government Engineering College, Thrissur
Status: Hidden
Slug: gec_thrissur
Container: True
Featured_img: https://upload.wikimedia.org/wikipedia/commons/5/5e/Gec_thrissur.jpg
Gallery: 20190213_gec_thrissur
Gallery_items: 6
Gallery_links: Ranjith Siji talking about Wikimedia Projects at Government Engineering College Thrissur 02.jpg|Kannan V M assisting students at Wikipedia Workshop, Government Engineering College, Thrissur.jpg|Students at Wikipedia Content Editing Workshop, Government Engineering College Thrissur 01.jpg|Ranjith Siji introducing Wikidata to students at Government Engineering College Thrissur.jpg|Ambady presenting wikidata tools at Conted Editing Workshop at GEC Thrissur.jpg|Content Editing Workshop at GEC Thrissur 03.jpg

<div class="steps">
A Wikipedia workshop was conducted at Government Engineering College, Thrissur. The workshop started at 10am on 13th February 2018. 
</div>

## Highlights

  Item           | Description
  -------------  | -------------
  Event          | Wikidata Workshop
  Date           | February 13, 2019
  Venue          | Government Engineering College (GEC), Thrissur
  Location       | Ramavarmapuram, Thrissur District, Kerala
  Coordinators   | Ranjith Siji, Ambady Anand S
  Participants   | Computer Science and Engineering Department, GEC Thrissur
  Organized by   | Wikimedians of Kerala User Group
  New Users      | 64
  Total Edits    | 148


## Team

### Coordinators
* Ranjith Siji
* Ambady Anand S
* Kannan V M

### Volunteers
* Atulnair2202
* Dudududuel
* Fhdwiki
* Laya rachel prasad
* Vivxk
* abhinavmanoj
* Sreerajavk
* Yathuwiki
* Swathy m68
* Anna1729
* Niyaskc
* Harshaswarrier
* 98philips
* Shilpaswiki
* Sreerajavk
* Maria Roy

### Participants
* Abhirami k b
* Anjusha km
* Alanmichael123
* Harikrishnan K A
* Its anandu
* Midhun132
* Thomas George illiparambil
* Nabeel Parayil
* Lordson Lloyd
* Dennisputhiyedath
* SniperBeast
* Aneesaqib
* Athul.MR
* Nikhil Prasad Thrissur
* Abhijith PJ
* Nandakumari N
* Moji V Shajan
* Rosemerin Justin
* Lakshmi nandakumar menon
* Shabanaasmiks
* Kanthidineshkumark
* Senna mariya pius
* MenonMeera98
* Flower Mariya
* Leslie.aloysius
* Sneja N Joshy
* Joshua Shaji
* Arunanil01
* Amarjith01
* Sarthakanil99
* Navani888
* Ajay Ram K
* HadiqR
* Harikrishnanru04
* Safuvan024
* Smijin A B
* Amalfra038
* writercode
* Vibinefron
* UMAR RISWAN AP
* Sravan S Sivan
* Sreerag2019
* Anirudhkan
* Slhwiki
* Pranav98
* RAHUL CR10
* Sudhikrishnan MP
* Hemandvichu
