Title: Wikidata Workshop at Government Engineering College, Idukki
Status: Hidden
Slug: gec_idukki
Container: True
Featured_img: https://upload.wikimedia.org/wikipedia/commons/thumb/7/75/Govt_Engineering_College_Idukki_Campus_DSC_3349.jpg/1024px-Govt_Engineering_College_Idukki_Campus_DSC_3349.jpg
Gallery: 20190216_gec_idukki
Gallery_items: 5
Gallery_links: Mujeeb_Rahman_introducing_Wikipedia_at_Wikidata_Workshop_at_GEC_Idukki_DSC_3390.jpg|Wikidata_Workshop_at_GEC_Idukki_DSC_3385.jpg|Wikidata_Workshop_at_GEC_Idukki_DSC_3399.jpg|Wikidata_Workshop_at_GEC_Idukki_DSC_3405.jpg|Wikidata_Workshop_at_GEC_Idukki_DSC_3413.jpg

<div class="steps">
A Wikipedia and Wikidata workshop was conducted at Government Engineering College, Idukki. The workshop started at 10 am on 16th February 2019. Mujeeb Rahman introduced Wikipedia, Wikimedia Commons and other Wikimedia projects in the morning session and a hands-on traing in Wikidata and SPARQL was led by Ranjith Siji. Students from Computer Science, Electronics and communication branches participated in this event. The event was organized by FOSS cell at the college organized.
</div>

## Highlights

  Item           | Description
  -------------  | -------------
  Event          | Wikipedia and Wikidata Workshop
  Date           | February 16, 2019
  Venue          | Government Engineering College (GEC), Idukki
  Location       | Painavu, Idukki District, Kerala
  Coordinators   | Ranjith Siji, Mujeeb Rahman
  Participants   | Computer Science and Engineering Department, GEC Idukki
  Organized by   | Wikimedians of Kerala User Group
  New Users      | 74
  Total Edits    | 266


## Team

### Coordinators
* Ranjith Siji
* Mujeeb Rahman

### Volunteers
* Minumumthas
* Asmida Nharakodan
* Jeffyjeffz
* Farhanlatheefkt
* MUBASH M

### Participants
* Anirudht.d
* Jamshina Ashraf
* Induja Sivan
* JesnaTJ
* Kanchana shanmughum
* NEHA THADEUS
* Ammu sajeev
* Sulthu$
* Muthuvenkit
* Babitharaj B
* Alnacl
* Arjun.pankaj.1144
* Athira T J
* Shamil Shamsudheen P V
* Silpachandrabose
* Athiravijayansujatha
* Rakendhu N R
* Aswinvijayan1111
* AjayJoseAji
* Kirankkottarathil
* Akshay k das
* Basilmirshabk
* Alfredbabu21
* ASHNA GRACE
* Mq.xz
* Sreelakshmi sreemi
* Niveditha.T.P
* Akshai Sonu
* Praveena Pradeep
* JabirWiKi
* Najula M N
* Anumol tm
* Mehnas P B
* Vrinda Mohan
* Vishnuprasadvisi
* Sagarrbrspb077
* Aswathyvipanchika
* Abhinandha P
* SILPA VINOD
* Ameer Badushah T
* ARYA ACHUTHAN
* Athulya. A
* Devika T N
* Sarannyabinu
* Parvathy ks
* Vishnump123
* Sana V H
* Anwarfarooq45
* Ashna Shajan
* Anupama Chandran
* Niveth J Unnithan
* Aakarsh B
* Sanjayvrsanju1998
* Bineesh n
* Mohamedyoonus47
* Ashar Jaman
* Anjali KP
* ABEER TP
* YADHUKRISHNAN C J
* Anurudhsoman
* Rasheedayappally
* SALMAN BIN ASHRAF
* Kmunaismpm
* Dhigish Giressh
* Nirmal98
* Aleena saji
* Fahadvar
* Amal11599
* Nisari k k
