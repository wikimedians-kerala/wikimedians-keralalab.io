Title: Introduction to Wikipedia at Government Engineering College, Palakkad
Status: Hidden
Slug: gec_palakkad
Container: True
Featured_img: https://upload.wikimedia.org/wikipedia/commons/f/f7/Wikipedia_Training_2.jpg
Gallery: 20190208_gec_palakkad
Gallery_items: 2
Gallery_links: Wikipedia Training 2.jpg|Wikipedia Training 1.jpg

<div class="steps">
Introduction to Wikipedia Demonstration was conducted at Govt Engineering College Palakkad, Sreekrishnapuram on 8th February 2019. This workshop was conducted in association with FOSS cell at the college. Mujeeb Rahman taken the demonstration and introduced Wikipedia, Commons and other Wikimedia projects to the students in the College.
</div>

## Highlights

  Item           | Description
  -------------  | -------------
  Event          | Introduction to Wikipedia
  Date           | February 8, 2019
  Venue          | Government Engineering College (GEC), Palakkad
  Location       | Sreekrishnapuram, Palakkad District, Kerala
  Coordinators   | Mujeeb Rahman, Hemang Mohan
  Participants   | Computer Science and Engineering Department, GEC Palakkad
  Organized by   | Wikimedians of Kerala User Group

## Team

### Coordinators
* Mujeeb Rahman
* Hemang Mohan
