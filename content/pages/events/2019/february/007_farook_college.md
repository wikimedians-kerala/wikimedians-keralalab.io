Title: Wikipedia Workshop at Farook College, Kozhikkode
Status: Hidden
Slug: farook_college
Container: True
Featured_img: https://upload.wikimedia.org/wikipedia/commons/thumb/6/63/Farook_College_Kozhikode_physics_department.jpg/1024px-Farook_College_Kozhikode_physics_department.jpg
Gallery: 20190214_farook_college
Gallery_items: 6
Gallery_links: Ranjith Siji talking about Wikipedia at Farook College Kozhikode.jpg|Ranjith Siji talking about Wikimedia projects at Farook College Kozhikode.jpg|Wikimedia Workshop at Farook College Kozhikode 5.jpg|Ranjith Siji talking about Wiki Loves Women at Farook College Kozhikode.jpg|Wikimedia Workshop at Farook College Kozhikode 4.jpg|Wikimedia Workshop at Farook College Kozhikode 3.jpg

<div class="steps">
Wikipedia Workshop was conducted at Farook College, Calicut.
</div>

## Highlights

  Item           | Description
  -------------  | -------------
  Event          | Wikipedia Workshop
  Date           | February 14, 2019
  Venue          | Farook College, Kozhikkode
  Location       | Feroke, Kozhikkode District, Kerala
  Coordinators   | Ranjith Siji, Ambady Anand S, Kannan V M, Akbar Ali
  Participants   | Malayalam Department, Farook College
  Organized by   | Wikimedians of Kerala User Group
  New Users      | 23
  Total Edits    | 148
  
## Team
  
### Coordinators
* Ranjith Siji
* Ambady Anand S
* Kannan V M
* Akbar Ali

### Participants
* Ranishan
* Zakiyajafarkt
* Irfanaissath
* Hasna vp
* Anzila parveen
* Anisanoufal
* Vijisha vinod
* Rahul manappat
* Hida Ibthisam Sakan
* Anaghasonu
* Asnameera
* Afrin kulangara
* Nuhmank
* Mahindileep
* Safna. K
* Chinnu19
* Shahananizam
* Hashimps
* Hiba Anas
* Shirinshameen
* Nayana Narda
* SUBISHA SUDHAKARAN
* Aswathichandran
