Title: Wikidata Workshop at St. Jospeh's College of Engineering and Technology, Palai
Status: Hidden
Container: True
Slug: sjcet_palai
Featured_img: https://upload.wikimedia.org/wikipedia/commons/thumb/b/bd/St._Joseph%27s_College_of_Engineering_and_Technology_Palai.jpg/1024px-St._Joseph%27s_College_of_Engineering_and_Technology_Palai.jpg
Gallery: 20190207_sjcet_palai
Gallery_items: 7
Gallery_links: Ranjith Siji talking about unstructured data at Wikidata Workshop, St. Joseph's College of Engineering and Technology Palai.jpg|Students at Wikidata Workshop, St. Joseph's College of Engineering and Technology, Palai.jpg|Ambadyanands taking Wikidata workshop at St. Joseph's College of Engineering and Technology, Palai DSC 2222.jpg|Kannan at Wikidata workshop at St. Joseph's College of Engineering and Technology, Palai DSC 2237.jpg|Ambadyanands taking Wikidata workshop at St. Joseph's College of Engineering and Technology, Palai DSC 2188.jpg|Wikidata workshop at St. Joseph's College of Engineering and Technology, Palai DSC 2201.jpg|Wikidata workshop at St. Joseph's College of Engineering and Technology Palai 03.jpg

<div class="steps">
A Wikidata workshop is conducted at St. Joseph's College of Engineering and Technology, Palai on 7th February 2019. This workshop is associated with the Tech fest at the college, Astra 2019. The workshop was started at 10AM and ended at 5PM. 30 Students participated in the programme. 
</div>

## Highlights

  Item           | Description
  -------------  | -------------
  Event          | Wikidata Workshop
  Date           | February 7, 2019
  Venue          | St. Jospeh's College of Engineering and Technology (SJCET), Palai
  Location       | Palai, Kottayam District, Kerala
  Coordinators   | Ranjith Siji, Ambady Anand S, Kannan V M
  Participants   | Computer Science and Engineering Department, SJCET Palai
  Organized by   | Wikimedians of Kerala User Group
  New Users      | 20
  Total Edits    | 112


## Team

### Coordinators
* Ranjith Siji
* Ambady Anand S
* Kannan V M

### Volunteers
* Abraham Raji
* Alex Joseph
* Chrissie Aldo

### Participants
* Augustine1998
* Allen S kunnel
* Anukool M M
* Rahitha R
* Ajithkr.akr
* Shalu V Suresh
* Sharonshibu123
* JishnumohanRDX
* Shinu S Mannil
* Christa mariam
* Aiswarya J Anil
* Gokuls24
* Shravan saj
* Mithunzachariavarghese
* Rsumesh396
* Arjun j warrior
* Aadarsh C Vinod
* Gouth123am
* Shal123om
* Jobel Jose George
