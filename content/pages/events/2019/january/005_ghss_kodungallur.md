Title: Introduction to Wikipedia at Government Higher Secondary School, Kodungallur
Status: Hidden
Slug: ghss_kodungallur
Container: True
Featured_img: https://upload.wikimedia.org/wikipedia/commons/thumb/8/84/Ghss-kodungallur.jpg/1024px-Ghss-kodungallur.jpg
Gallery: 20190126_ghss_kodungallur
Gallery_items: 6
Gallery_links: GHSS Kodungallur Wikipedia class photo 2.jpg|Wikipedia workshop at ghsskodungallur Students.jpg|GHSS Kodungallur Wikipedia class photo 4.jpg|Wikipedia workshop at ghsskodungallur kannan.jpg|Wikipedia workshop at ghsskodungallur Ambady.jpg|GHSS Kodungallur Wikipedia class photo 1.jpg

<div class="steps">
Introductory training on wikipedia is given to the students of GHSS Kodungallur School. Fifteen students from 7th, 8th and 9th standard were present for the workshop. The program was conducted with support from GHSS Kodungallur.
</div>

## Highlights

  Item           | Description
  -------------  | -------------
  Event          | Introduction to Wikipedia
  Date           | January 26, 2019
  Venue          | Government Higher Secondary School (GHSS), Kodungallur
  Location       | Kodungallur, Thrissur District, Kerala
  Coordinators   | Mujeeb Rahman, Kannan V M, Ambady Anand S, Medha
  Participants   | High School Students
  Organized by   | Wikimedians of Kerala User Group


## Team

### Coordinators
* Mujeeb Rahman
* Kannan V M
* Ambady Anand S
* Medha
