Title: Introduction to Wikipedia at Women's Polytechninc College, Nedupuzha
Status: Hidden
Slug: wpc_nedupuzha
Container: True
Featured_img: https://upload.wikimedia.org/wikipedia/commons/thumb/d/d5/Nedupuzha_women%27s_poly_technique_electronics_block.jpg/1024px-Nedupuzha_women%27s_poly_technique_electronics_block.jpg
Gallery: 20190116_wpc_nedupuzha
Gallery_items: 2
Gallery_links: Mujeeb Rahman K introducing Malayalam Wikipedia at Women's Polytechnic College Nedupuzha IMG 20190116 121234.jpg|Mujeeb Rahman K introducing Malayalam Wikipedia at Women's Polytechnic College Nedupuzha IMG 20190116 121145.jpg

<div class="steps">
An introduction to Wikipedia and other Wikimedia projects is given to the students of Women's Polytechnic College, Nedupuzha on 16th January 2019. After introducing Malayalam Wikipedia to the students, Mujeeb Rahman also demonstrated how to contribute to Wikipedia, Wikimedia Commons, Wikisource and Wikiquotes. A total of 40 students participated in the program.
</div>

## Highlights

  Item           | Description
  -------------  | -------------
  Event          | Introduction to Wikipedia
  Date           | January 16, 2019
  Venue          | Women's Polytechnic College (WPC), Nedupuzha
  Location       | Nedupuzha, Thrissur District, Kerala
  Coordinators   | Mujeeb Rahman, Ambady Anand S
  Participants   | Computer Engineering Department
  Organized by   | Wikimedians of Kerala User Group


## Team

### Coordinators
* Mujeeb Rahman
* Ambady Anand S
