Title: Introduction to Wikipedia at Government Higher Secondary School, Chowara
Status: Hidden
Slug: ghss_chowara
Container: True
Featured_img: https://upload.wikimedia.org/wikipedia/commons/1/1e/Wiki_introduction_chovvara_Ghss_1.jpg
Gallery: 20190129_ghss_chowara
Gallery_items: 2
Gallery_links: Wiki introduction chovvara Ghss 1.jpg|Wiki introduction chovvara Ghss 2.jpg

<div class="steps">
An Introductory training about Wikipedia and other Wikimedia projects was done at Govt. Higher Secondary School Chowara on 29th January 2019. The event was led by Mujeeb Rahman K. After the introduction Mujeeb demonstrated how to contribute to Wikipedia and other Wikimedia projects and the importance of doing that. 40 students from high school and higher secondary school was participated on the event.
</div>

## Highlights

  Item           | Description
  -------------  | -------------
  Event          | Introduction to Wikipedia
  Date           | January 29, 2019
  Venue          | Government Higher Secondary School (GHSS), Chowara
  Location       | Chowara, Ernakulam District, Kerala
  Coordinators   | Mujeeb Rahman, Saranya C
  Participants   | High School Students
  Organized by   | Wikimedians of Kerala User Group


## Team

### Coordinators
* Mujeeb Rahman
* Saranya C
