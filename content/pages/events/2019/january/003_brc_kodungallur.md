Title: Introduction to Wikipedia at Block Resource Centre, Kodungallur
Status: Hidden
Slug: brc_kodungallur
Container: True
Featured_img: https://upload.wikimedia.org/wikipedia/commons/5/57/Teachers_meet_and_Wikimedia_Exhibition_at_Block_Resource_Center_Kodungallur_12.jpg
Gallery: 20190114_brc_kodungallur
Gallery_items: 3
Gallery_links: Wiki Adhyapakasangamam 2019 IMG 20190114 143431.jpg|Ranjith Siji talking at Wiki Adhyapakasangamam 2019 IMG 20190114 143555.jpg|Teachers meet and Wikimedia Exhibition at Block Resource Center Kodungallur 12.jpg

<div class="steps">
A Wikipedia demonstration was conducted at Block Resource Center, Kodungallur on 14th January 2019. This demonstration was conducted as a part of Wikisangamotsavam 2018. The event was organized in association with Kerala Infrastructure and Technology for Education(KITE) and Department of Education, Kerala Government. Teachers from various schools around Kodungallur participated in this programme. Ranjithsiji taken the introductory class about Wikipedia, Commons, Wikisource and other Wikimedia projects. 50 teachers participated in this workshop.
</div>

## Highlights

  Item           | Description
  -------------  | -------------
  Event          | Introduction to Wikipedia
  Date           | January 14, 2019
  Venue          | Block Resource Centre (BRC), Kodungallur
  Location       | Kodungallur, Thrissur  District, Kerala
  Coordinators   | Ranjith Siji, Kannan V M, Ambady Anand S, Manoj V
  Participants   | IT@School (KITE) Teachers
  Organized by   | Wikimedians of Kerala User Group

## Team

### Coordinators
* Ranjith Siji
* Kannan V M
* Ambady Anand S
* Manoj V
