Title: Introduction to Wikipedia at Government Higher Secondary School, Mathirappilly
Status: Hidden
Slug: ghss_mathirappilly
Container: True
Featured_img: https://upload.wikimedia.org/wikipedia/commons/5/59/Wiki_introduction_to_mahiripill_ghss_students.jpg
Gallery: 20190114_ghss_mathirappilly
Gallery_items: 2
Gallery_links: Wiki introduction to mahiripill ghss students.jpg|Wiki introduction mathirapilly ghss.jpg

<div class="steps">
An Introductory training about Wikipedia, Commons and Wikisource is given to the students in Govt Higher Secondary School, Mathirappilly on 14th January 2019. 36 students from High school are participated in the training. Mujeeb Rahman given an introduction about Wikipedia and wikimedia projects in this training. After that a demonstration of how to edit wikipedia and how to create a new article are done.
</div>

## Highlights

  Item           | Description
  -------------  | -------------
  Event          | Introduction to Wikipedia
  Date           | January 14, 2019
  Venue          | Government Higher Secondary School (GHSS), Mathirappilly
  Location       | Mathirappilly, Ernakulam District, Kerala
  Coordinators   | Mujeeb Rahman, Saranya C
  Participants   | High School Students
  Organized by   | Wikimedians of Kerala User Group


## Team

### Coordinators
* Mujeeb Rahman
* Saranya C
