Title: Introduction to Wikipedia at Kodungallur Kunjhikkuttan Thampuran Memorial College, Pullut
Status: Hidden
Slug: kktm_college
Container: True
Featured_img: https://upload.wikimedia.org/wikipedia/commons/thumb/6/68/KKTM_College_Kodungallur_buildings.jpg/1024px-KKTM_College_Kodungallur_buildings.jpg
Gallery: 20190114_kktm_college
Gallery_items: 3
Gallery_links: Ranjith Siji talking at Wiki Yuvasangamam 2019 IMG 20190114 110447.jpg|Wiki Yuvasangamam 2019 IMG 20190114 122947.jpg|Manoj talking at Wiki Yuvasangamam 2019 IMG 20190114 120920.jpg

<div class="steps">
A Wikipedia demonstration was conducted on Kodungallur Kunjikkuttan Thampuran College, Kodungallur on 14th January 2019. The demonstration was conducted as a part of Wikisangamotsavam 2018. The event was organized in association with Malayalam Department in the College. Students from Malyalam department participated in this programme. Ranjithsiji taken the introductory class about Wikipedia, Commons, Wikisource and other Wikimedia projects. 30 students participated in this workshop.
</div>

## Highlights

  Item           | Description
  -------------  | -------------
  Event          | Introduction to Wikipedia
  Date           | January 14, 2019
  Venue          | Kodungallur Kunjhikkuttan Thampuran Memorial (KKTM) College, Pullut
  Location       | Kodungallur, Thrissur District, Kerala
  Coordinators   | Ranjith Siji, Kannan V M, Ambady Anand S, Manoj V
  Participants   | Malayalam Department, KKTM College
  Organized by   | Wikimedians of Kerala User Group

## Team

### Coordinators
* Ranjith Siji
* Kannan V M
* Ambady Anand S
* Manoj V
