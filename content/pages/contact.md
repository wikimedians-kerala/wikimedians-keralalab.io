Title: Contact
Container: True

  Item              | Description
  ------------------| -------------
  Official Website  | [wikimediakerala.in](https://wikimediakerala.in)
  E-mail Address    | [wikimedianskerala@gmail.com](mailto:wikimedianskerala@gmail.com)
  Contact Number    | +91-9446541729
  Telegram          | [@wikimedianskerala](https://t.me/wikimedianskerala)
  Twitter           | [@wikimediakerala](https://twitter.com/wikimediakerala)
  Facebook          | [@wikimedianskerala](https://www.facebook.com/wikimedianskerala/)
