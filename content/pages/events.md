Title: Events
Summary: Events organized by Wikimedian of Kerala User Group
Container: True

## 2019

### February
* [17/02/2019] [Introduction to Wikipedia at College of Engineering, Karunagappally](2019/february/coe_karunagappally)
* [16/02/2019] [Wikidata Workshop at Government Engineering College, Idukki](2019/february/gec_idukki)
* [15/02/2019] [Introduction to Wikipedia at Ahalia School of Engineering and Technology, Palakkad](2019/february/aset_palakkad)
* [14/02/2019] [Wikipedia Workshop at Farook College, Kozhikkode](2019/february/farook_college)
* [13/02/2019] [Wikidata Workshop at Government Engineering College, Thrissur](2019/february/gec_thrissur)
* [08/02/2019] [Wikipedia Workshop at Sree Sankaracharya University of Sanskrit, Kalady](2019/february/ssus_kalady)
* [08/02/2019] [Introcution to Wikipedia at Government Engineering College, Palakkad](2019/february/gec_palakkad)
* [07/02/2019] [Wikidata Workshop at St. Jospeh's College of Engineering and Technology, Palai](2019/february/sjcet_palai)
* [04/02/2019] [Introduction to Wikipedia at Eranad Knowledge City, Manjeri](2019/february/ekc_manjeri)
* [03/02/2019] [Exhibition and stall at Dhishna Tech Fest, Cochin University of Science and Technology, Kochi](2019/february/cusat_kochi)

### January
* [29/01/2019] [Introduction to Wikipedia at Government Higher Secondary School, Chowara](2019/january/ghss_chowara)
* [26/01/2019] [Introduction to Wikipedia at Government Boys Higher Secondary School, Kodungallur](2019/january/ghss_kodungallur)
* [16/01/2019] [Introduction to Wikipedia at Women's Polytechnic College, Nedupuzha](2019/january/wpc_nedupuzha)
* [14/01/2019] [Introduction to Wikipedia at Block Resource Centre, Kodungallur](2019/january/brc_kodungallur)
* [14/01/2019] [Introduction to Wikipedia at Kodungallur Kunjikuttan Thampuran Memorial Government College, Pullut](2019/january/kktm_college)
* [14/01/2019] [Introduction to Wikipedia at Government Higher Secondary School, Mathirappilly](2019/january/ghss_mathirappilly)
