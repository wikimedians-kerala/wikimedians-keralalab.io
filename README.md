Official Website of Wikimedians of Kerala User Group

Powered by [Pelican](https://blog.getpelican.com/)

* Template used: Evie (Ported to Pelican)
* Template site: [https://evie.undraw.co](https://evie.undraw.co)
* Template license: MIT (For more details see the LICENSE file)
* Template details: An MIT licensed template bundled with a minimal style guide to build websites faster, especially combined with illustrations from [unDraw](https://undraw.co). It is extemely lightweight, customizable and works perfectly on modern browsers.
* Template preview: ![Evie Landing](https://github.com/anges244/evie/blob/master/docs/images/preview_landing.png)
